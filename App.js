import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Image, FlatList } from 'react-native';
import React, { useState, useEffect } from 'react';

export default function App() {

  const [name, setName] = useState(null);
  const [img, setImg] = useState(null);
  const [list, setList] = useState(null);

  const getAllCocktail = () => {
    return fetch('https://www.thecocktaildb.com/api/json/v1/1/search.php?f=a')
        .then((response) => response.json())
        .then((json) => {
          setList(json.drinks)
          console.log(json.drinks[0].strDrinkThumb)
        })
        .catch((error) => {
          console.error(error);
        });
  };

  const Item = ({title}) => (
    <View style={styles.item}>
      {console.log("img:" + title.strDrinkThumb)}
        <Image style={styles.tinyLogo} source={{uri: title.strDrinkThumb}}/>
        <Text style={styles.name}>{title.strDrink}</Text>
    </View>
);

  const renderItem = ({ item }) => (
      <Item title={item} />
  );


  useEffect(() => {
    (async () => {
      getAllCocktail();
    })();
  }, []);


  return (
    <View style={styles.container}>
      <View style={styles.menubar}>      
        <Text style={styles.menu}>Shake It</Text>
      </View>
      <View>      
        <FlatList data={list} renderItem={renderItem}/>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 40,
  },
  tinyLogo: {
    width: 300,
    height: 300,
  },
  item: {
    backgroundColor: '#D3D3D3',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    borderRadius: 10,
    textAlign: 'center',
    justifyContent: 'center',
},
name: {
  textAlign: 'center'
},
menu: {
  fontWeight: 'bold',
  fontSize: 40,
  fontFamily: "Cochin",
  paddingTop: 40
},
menubar: {
}
});
